export interface SnackbarData {
  message: string;
  status: 'success' | 'info' | 'error';
}
