export interface Holding {
  tickerLable: string;
  description: string;
  current_price: number;
  purchase_price: number;
  quantity: number;
  total_value: number;
  profit_or_loss: number;
}
