export interface Exchange {
  label: string;
  endpoint: string;
  status: string;
}
