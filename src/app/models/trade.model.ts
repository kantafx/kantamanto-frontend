export interface Trade {
  symbol: string;
  side: string;
  purchase_price: number;
  quantity: number;
  total_value: number;
  status: string;
  profit_or_loss: number;
  customer: string;
  portfolio: string;
}
