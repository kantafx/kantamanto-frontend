import { Timestamp } from "rxjs";

export interface Order {
  tickerLable: string;
  side: string;
  quantity: number;
  portfolio: {name: string, id: number};
  orderType: string;
  price?: number;
}

export interface OrderRequest {
  tickerLable: string;
  side: string;
  quantity: number;
  portfolioId: number;
  orderType: string;
  price?: number;
}

export interface PendingOrder {
    ticker: string;
    exchangeId: string;
    exchangeName: string;
    orderType: string;
    side: string;
    quantity: number;
    price: number;
    createdAt: string;
}

export interface Ticker {
  tickerLable: string,
  description: string
}

export interface Stock {
  ticker: Ticker,
  value: number,
  quantity: number
}

export interface CurrentPrice {
  tickerLable: string,
		askPrice: number
}
