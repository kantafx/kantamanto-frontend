import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientAppComponent } from './pages/client/client-app/client-app.component';
import { ClientRegisterComponent } from './pages/client/client-register/client-register.component';
import { ClientLoginComponent } from './pages/client/client-login/client-login.component';
import { ClientDashboardComponent } from './pages/client/client-dashboard/client-dashboard.component';
import { ClientTradeComponent } from './pages/client/client-trade/client-trade.component';

import { AdminAppComponent } from './pages/admin/admin-app/admin-app.component';
import { AdminLoginComponent } from './pages/admin/admin-login/admin-login.component';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { AdminTradesComponent } from './pages/admin/admin-trades/admin-trades.component';
import { AdminConfigureSystemComponent } from './pages/admin/admin-configure-system/admin-configure-system.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { MarketComponent } from './pages/market/market.component';
import { IsAuthenticatedGuard } from './auth/is-authenticated.guard';
import { ClientAuthenticatedGuard } from './auth/client-authenticated.guard';
import { AdminAuthenticatedGuard } from './auth/admin-authenticated.guard';
import { NotAuthenticatedGuard } from './auth/not-authenticated.guard';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
    canActivate: [NotAuthenticatedGuard],
  },
  {
    path: 'client',
    component: ClientAppComponent,
    children: [
      { path: '', redirectTo: '/', pathMatch: 'full' },
      {
        path: 'register',
        component: ClientRegisterComponent,
        canActivate: [NotAuthenticatedGuard],
      },
      {
        path: 'login',
        component: ClientLoginComponent,
        canActivate: [NotAuthenticatedGuard],
      },
      {
        path: 'dashboard',
        component: ClientDashboardComponent,
        canActivate: [IsAuthenticatedGuard, ClientAuthenticatedGuard],
      },
      {
        path: 'trade',
        component: ClientTradeComponent,
        canActivate: [IsAuthenticatedGuard, ClientAuthenticatedGuard],
      },
      {
        path: 'market',
        component: MarketComponent,
        canActivate: [IsAuthenticatedGuard, ClientAuthenticatedGuard],
      },
    ],
  },
  {
    path: 'admin',
    component: AdminAppComponent,
    children: [
      { path: '', redirectTo: '/', pathMatch: 'full' },
      {
        path: 'login',
        component: AdminLoginComponent,
        canActivate: [NotAuthenticatedGuard],
      },
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
        canActivate: [IsAuthenticatedGuard, AdminAuthenticatedGuard],
      },
      {
        path: 'trades',
        component: AdminTradesComponent,
        canActivate: [IsAuthenticatedGuard, AdminAuthenticatedGuard],
      },
      {
        path: 'configure-system',
        component: AdminConfigureSystemComponent,
        canActivate: [IsAuthenticatedGuard, AdminAuthenticatedGuard],
      },
      {
        path: 'market',
        component: MarketComponent,
        canActivate: [IsAuthenticatedGuard, AdminAuthenticatedGuard],
      },
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
