import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTradeOrderComponent } from './preview-trade-order.component';

describe('PreviewTradeOrderComponent', () => {
  let component: PreviewTradeOrderComponent;
  let fixture: ComponentFixture<PreviewTradeOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviewTradeOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreviewTradeOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
