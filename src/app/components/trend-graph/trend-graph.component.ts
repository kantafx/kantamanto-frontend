import { Component, Input, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { BehaviorSubject } from 'rxjs';
import { MarketDataService } from 'src/app/services/market-data.service';
import { default as Annotation } from 'chartjs-plugin-annotation';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-trend-graph',
  templateUrl: './trend-graph.component.html',
  styleUrls: ['./trend-graph.component.css'],
})
export class TrendGraphComponent {
  @Input()
  symbol!: string;

  constructor(private marketDataService: MarketDataService) {
    Chart.register(Annotation);
  }

  graphData$ = new BehaviorSubject<number[]>([]);
  num: string = '';

  ngOnInit() {
    if (this.symbol) {
      this.marketDataService.getTickerData().subscribe((response) => {
        // console.log(response);

        this.graphData$.next(response[this.symbol]);
        this.chart?.update();
        // console.log(response[this.symbol]);
        console.log(this.graphData$.value);
      });
    }
  }

  firstCopy = false;

  // data
  public lineChartData: Array<number> = [1, 8, 49];

  public labelMFL: Array<any> = [
    { data: this.graphData$.value, label: this.symbol },
  ];

  // labels
  public lineChartLabels: Array<any> = [
    '2018-01-29 10:00:00',
    '2018-01-29 10:27:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
    '2018-01-29 10:28:00',
  ];

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;
}
