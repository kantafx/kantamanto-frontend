import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NewPortfolio } from 'src/app/models/portfolio.model';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-create-portfolio',
  templateUrl: './create-portfolio.component.html',
  styleUrls: ['./create-portfolio.component.css'],
})
export class CreatePortfolioComponent {
  constructor(
    public dialogRef: MatDialogRef<CreatePortfolioComponent>,
    private portfolioService: PortfolioService,
    private uiService: UiService
  ) {}

  form: NewPortfolio = {
    portfolioName: '',
    amount: 0,
    email: JSON.parse(localStorage.getItem('user') as string).email,
  };

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    if (!this.form.portfolioName) {
      alert('Invalid data.');
      return;
    }

    this.portfolioService.createPortfolio({ ...this.form }).subscribe({
      next: (response) => {
        if (response) {
          this.portfolioService.init();
          this.uiService.showSnackbar({
            message: response.message,
            status: 'success',
          });
        }
      },
      error: (error) => {
        this.uiService.showSnackbar({
          message: error.error.message,
          status: 'error',
        });
      },
    });
    this.onClose();
  }
}
