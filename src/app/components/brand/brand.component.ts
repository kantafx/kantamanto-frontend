import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css'],
})
export class BrandComponent {
  constructor(private router: Router) {}

  @Input() dark?: boolean = true;

  getCurrentRoute() {
    return this.router.url;
  }
}
