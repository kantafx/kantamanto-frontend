import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { SnackbarData } from './models/snackbar.model';
import { AuthService } from './services/auth.service';
import { UiService } from './services/ui.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'TradingApp';

  snackbarSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private uiService: UiService,
    private _snackBar: MatSnackBar
  ) {
    this.snackbarSubscription = this.uiService
      .onShowSnackbar()
      .subscribe((value) => {
        this.showSnackbar({ ...value });
      });
  }

  ngOnInit() {
    this.authService.setIsLoggedIn(!!localStorage.getItem('lgn'));
  }

  showSnackbar(data: SnackbarData) {
    this._snackBar.open(data.message, 'Close', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 3000,
    });
  }
}
