import { TestBed } from '@angular/core/testing';

import { ClientAuthenticatedGuard } from './client-authenticated.guard';

describe('ClientAuthenticatedGuard', () => {
  let guard: ClientAuthenticatedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ClientAuthenticatedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
