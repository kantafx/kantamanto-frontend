import { Component, OnInit } from '@angular/core';
import { UiService, Focus } from 'src/app/services/ui.service';
import { Subscription } from 'rxjs';
import { Exchange } from 'src/app/models/exchange.model';
import { Stock } from 'src/app/models/stock.model';

const EXCHANGE_DATA: Exchange[] = [];

const STOCK_DATA: Stock[] = [];

@Component({
  selector: 'app-admin-configure-system',
  templateUrl: './admin-configure-system.component.html',
  styleUrls: ['./admin-configure-system.component.css'],
})
export class AdminConfigureSystemComponent implements OnInit {
  exchangeCols: string[] = ['label', 'endpoint', 'status', 'action'];
  exchanges = EXCHANGE_DATA;

  stockCols: string[] = ['symbol', 'description', 'action'];
  stocks = STOCK_DATA;

  expandedItem: Focus = 'exchange';
  subscription: Subscription;

  constructor(private uiService: UiService) {
    this.subscription = this.uiService
      .onSwitchFocus()
      .subscribe((value) => (this.expandedItem = value));
  }

  ngOnInit(): void {}

  onExpand(str: Focus) {
    this.uiService.switchFocus(str);
  }
}
