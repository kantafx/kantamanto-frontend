import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConfigureSystemComponent } from './admin-configure-system.component';

describe('AdminConfigureSystemComponent', () => {
  let component: AdminConfigureSystemComponent;
  let fixture: ComponentFixture<AdminConfigureSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminConfigureSystemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminConfigureSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
