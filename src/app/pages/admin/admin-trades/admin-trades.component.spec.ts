import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTradesComponent } from './admin-trades.component';

describe('AdminTradesComponent', () => {
  let component: AdminTradesComponent;
  let fixture: ComponentFixture<AdminTradesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTradesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
