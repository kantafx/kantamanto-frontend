import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterData } from 'src/app/models/auth.model';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-client-register',
  templateUrl: './client-register.component.html',
  styleUrls: ['./client-register.component.css'],
})
export class ClientRegisterComponent {
  ngOnInit() {
    document.body.classList.add('u-body-bg-light-auth');
  }

  ngOnDestroy() {
    document.body.classList.remove('u-body-bg-light-auth');
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private uiService: UiService
  ) {}

  //form handling
  form: RegisterData = {
    name: '',
    email: '',
    password: '',
  };

  confirmPassword: string = '';

  errorMessage?: String;

  onSubmit(): void {
    if (this.form.password !== this.confirmPassword) {
      this.uiService.showSnackbar({
        message: 'Passwords do not match',
        status: 'error',
      });
      return;
    }

    if (!this.form.email || !this.form.password || !this.form.name) {
      this.uiService.showSnackbar({
        message: 'All fields are required',
        status: 'error',
      });
      return;
    }

    if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(this.form.email)) {
      this.uiService.showSnackbar({
        message: 'Invalid email address',
        status: 'error',
      });
      return;
    }

    this.authService.register({ ...this.form }).subscribe({
      next: (response) => {
        localStorage.setItem('user', JSON.stringify(response));
        localStorage.setItem('lgn', 'client');
        this.router.navigate(['/client/dashboard']);
        this.authService.setIsLoggedIn(true);
        localStorage.setItem('portfolio', '0');
      },
      error: (error) =>
        this.uiService.showSnackbar({
          message: error.error.message,
          status: 'error',
        }),
    });
  }
}
