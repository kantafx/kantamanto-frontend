import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PreviewTradeOrderComponent } from 'src/app/components/preview-trade-order/preview-trade-order.component';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { Observable, of, take } from 'rxjs';
import { TickerService } from 'src/app/services/ticker.service';
import { Order } from 'src/app/models/order.model';
import { ExistingPortfolio } from 'src/app/models/portfolio.model';
import { HoldingService } from 'src/app/services/holding.service';

@Component({
  selector: 'app-client-trade',
  templateUrl: './client-trade.component.html',
  styleUrls: ['./client-trade.component.css'],
})
export class ClientTradeComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private portfolioService: PortfolioService,
    private tickerService: TickerService,
    private holdingService: HoldingService
  ) {}

  userPortfolios$: Observable<ExistingPortfolio[]> = of([]);
  currentPrices$: Observable<any[]> = of([]);

  tickers$: Observable<any> = of([]);

  form: Order = {
    tickerLable: 'AMZN',
    side: 'BUY',
    quantity: 0,
    portfolio: {
      name: '',
      id: 0,
    },
    orderType: 'MARKET',
    price: 0,
  };

  prices = {
    ask: 0,
    bid: 0,
    last: 0,
  };

  updatePrices() {
    this.currentPrices$.forEach((item) => {
      item.forEach((e) => {
        if (e.tickerLable == this.form.tickerLable) {
          this.prices.ask = e.askPrice.toFixed(3);
          this.prices.bid = e.bidPrice.toFixed(3);
          this.prices.last = e.lastTradedPrice.toFixed(3);
        }
      });
    });
  }

  onPreviewOrder() {
    this.userPortfolios$.pipe(take(1)).subscribe((response) =>
      response.forEach((item) => {
        if (item.portfolioId == this.form.portfolio.id)
          this.form.portfolio.name = item.portfolioName;
      })
    );

    this.dialog.open(PreviewTradeOrderComponent, {
      data: {
        ...this.form,
      },
      width: '30vw',
    });
  }

  ngOnInit(): void {
    this.portfolioService.init();
    this.userPortfolios$ = this.portfolioService.getPortfolios();
    this.tickers$ = this.tickerService.getTickers();

    this.userPortfolios$.subscribe((response) => {
      if (response[0]) {
        this.form.portfolio.name =
          response[Number(localStorage.getItem('portfolio'))].portfolioName;
        this.form.portfolio.id =
          response[Number(localStorage.getItem('portfolio'))].portfolioId;
      }
    });

    this.holdingService.initCurrentPrices();
    this.currentPrices$ = this.holdingService.getAllPrices();

    this.tickers$.subscribe(() => {
      this.updatePrices();
    });
  }
}
