import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientTradeComponent } from './client-trade.component';

describe('ClientTradeComponent', () => {
  let component: ClientTradeComponent;
  let fixture: ComponentFixture<ClientTradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientTradeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
