import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginData } from 'src/app/models/auth.model';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-client-login',
  templateUrl: './client-login.component.html',
  styleUrls: ['./client-login.component.css'],
})
export class ClientLoginComponent {
  ngOnInit() {
    document.body.classList.add('u-body-bg-light-auth');
  }

  ngOnDestroy() {
    document.body.classList.remove('u-body-bg-light-auth');
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private uiService: UiService
  ) {}

  form: LoginData = {
    email: '',
    password: '',
  };

  onSubmit(): void {
    if (!this.form.email || !this.form.password) {
      this.uiService.showSnackbar({
        message: 'Please enter email and/or password',
        status: 'error',
      });
      return;
    }

    this.authService.login({ ...this.form }, 'client').subscribe({
      next: (response) => {
        localStorage.setItem('user', JSON.stringify(response));
        localStorage.setItem('lgn', 'client');
        this.router.navigate(['/client/dashboard']);
        this.authService.setIsLoggedIn(true);
        localStorage.setItem('portfolio', '0');
      },
      error: (error) =>
        this.uiService.showSnackbar({
          message: error.error.message,
          status: 'error',
        }),
    });
  }
}
