import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SnackbarData } from '../models/snackbar.model';

export type Focus = 'exchange' | 'stock';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  private focus: Focus = 'exchange';
  private focusSubject = new Subject<any>();

  private snackbarSubject = new Subject<SnackbarData>();

  constructor() {}

  switchFocus(to: Focus): void {
    this.focus = to;
    this.focusSubject.next(this.focus);
  }

  onSwitchFocus(): Observable<any> {
    return this.focusSubject.asObservable();
  }

  showSnackbar(data: SnackbarData) {
    this.snackbarSubject.next({
      ...data,
    });
  }

  onShowSnackbar(): Observable<SnackbarData> {
    return this.snackbarSubject.asObservable();
  }
}
