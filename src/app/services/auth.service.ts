import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginData, RegisterData } from '../models/auth.model';

type UserType = 'admin' | 'client';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': ['POST', 'GET', 'OPTIONS', 'DELETE', 'PUT'],
  }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private url = environment.apiUrl;
  private isLoggedIn$ = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  login(loginData: LoginData, userType: UserType): Observable<any> {
    return this.http.post<any>(
      `${this.url}/${userType}/login`,
      loginData,
      httpOptions
    );
  }

  register(registerData: RegisterData): Observable<any> {
    return this.http.post<any>(
      `${this.url}/client/register`,
      registerData,
      httpOptions,
      
    );
  }

  toggleIsLoggedIn(): void {
    this.isLoggedIn$.next(!this.isLoggedIn$.value);
  }

  isAuthenticated(): Observable<boolean> {
    const lgn: boolean = !!localStorage.getItem('lgn');
    return this.isLoggedIn$ || of(lgn);
  }

  setIsLoggedIn(value: boolean): void {
    this.isLoggedIn$.next(value);
  }
}
