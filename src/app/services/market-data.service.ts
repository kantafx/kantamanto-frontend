import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TickerData } from '../models/ticker-data.model';

@Injectable({
  providedIn: 'root',
})
export class MarketDataService {
  private source: EventSource | null = null;
  private tickerData = new BehaviorSubject<TickerData>({});

  constructor() {}

  getTickerData(): Observable<TickerData> {
    return this.tickerData;
  }

  createNewConnection(ticker: string): void {
    if (this.source) {
      this.source.close();
    }
    this.source = new EventSource(
      `http://localhost:8082/stream-flux/${ticker}`
    );
    this.source.onmessage = (evt) => {
      // console.log(evt.data);
      const data = JSON.parse(evt.data);

      for (const key in data) {
        const average = (data[key][0] + data[key][1]) / 2;
        const newData: TickerData = {};

        if (this.tickerData.value[key]) {
          // already exists!
          const prev = this.tickerData.value[key];
          if (prev.length == 10) prev.shift();
          prev.push(average);
          newData[key] = [...prev];
        } else {
          // new data!
          newData[key] = [average];
        }

        this.tickerData.next({
          ...this.tickerData.value,
          ...newData,
        });
      }
      // console.log(this.tickerData.value);
    };
  }
}

/*
const createNewConnection = (ticker) => {
  if(source) source.close();
  source = new EventSource(`http://localhost:8082/stream-flux/${ticker}`)
  source.onmessage = (evt) => console.log(evt.data)
}

const buttons = document.getElementsByClassName("tickerButtons")
for (const button of buttons) {
  button.onclick = (evt) => {
      createNewConnection(button.id)
  }
}
*/
