import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { ClientRegisterComponent } from './pages/client/client-register/client-register.component';
import { ClientAppComponent } from './pages/client/client-app/client-app.component';
import { ClientLoginComponent } from './pages/client/client-login/client-login.component';
import { ClientDashboardComponent } from './pages/client/client-dashboard/client-dashboard.component';
import { ClientTradeComponent } from './pages/client/client-trade/client-trade.component';

import { AdminAppComponent } from './pages/admin/admin-app/admin-app.component';
import { AdminLoginComponent } from './pages/admin/admin-login/admin-login.component';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { AdminTradesComponent } from './pages/admin/admin-trades/admin-trades.component';
import { AdminConfigureSystemComponent } from './pages/admin/admin-configure-system/admin-configure-system.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { MarketComponent } from './pages/market/market.component';

import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { TopbarComponent } from './components/topbar/topbar.component';
import { HoldingsComponent } from './components/holdings/holdings.component';
import { PreviewTradeOrderComponent } from './components/preview-trade-order/preview-trade-order.component';
import { BrandComponent } from './components/brand/brand.component';
import { CreatePortfolioComponent } from './components/create-portfolio/create-portfolio.component';

import { AuthService } from './services/auth.service';
import { IsAuthenticatedGuard } from './auth/is-authenticated.guard';
import { BrandNavComponent } from './components/brand-nav/brand-nav.component';
import { NgChartsModule } from 'ng2-charts';
import { TrendGraphComponent } from './components/trend-graph/trend-graph.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ClientRegisterComponent,
    ClientAppComponent,
    AdminAppComponent,
    AdminLoginComponent,
    ClientLoginComponent,
    HomepageComponent,
    MarketComponent,
    ClientDashboardComponent,
    AdminDashboardComponent,
    ClientTradeComponent,
    AdminTradesComponent,
    AdminConfigureSystemComponent,
    TopbarComponent,
    HoldingsComponent,
    PreviewTradeOrderComponent,
    BrandComponent,
    CreatePortfolioComponent,
    BrandNavComponent,
    TrendGraphComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule,
    MatGridListModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
    FormsModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    NgChartsModule,
  ],
  providers: [AuthService, IsAuthenticatedGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
